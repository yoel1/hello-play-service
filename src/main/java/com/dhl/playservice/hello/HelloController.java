package com.dhl.playservice.hello;

import play.mvc.Controller;
import play.mvc.Result;

public class HelloController extends Controller {

    public Result byName(String name) {
        return ok("hello " + name);
    }
}
